/*
 * Copyright (C) 2018 Gedare Bloom
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "cpu.h"
#include "memory.h"

struct cpu_context cpu_ctx;

int fetch( struct IF_ID_buffer *out )
{
	out->pc = cpu_ctx.PC;			//our inital instruction is found at first PC
	out->next_pc = out->pc + 4;		//increment pc by 4 to find next instruction 
	out->instruction = instruction_memory[out->pc]; //looks at instruction found in instruction memory
	
	return 0;
}

int decode( struct IF_ID_buffer *in, struct ID_EX_buffer *out )
{
	
	//Initalize opcode and funct codes 
	unsigned opcodeMask, funct3Mask, funct7Mask;
	
	
	opcodeMask = ((1 << 7) - 1) & in->instruction;
	
	//Switch statement to find and store opcode and funct code
	switch (opcodeMask) {
		case 0b0000011:
			//lw
			out->instruction[0] = 'l';
			out->instruction[1] = 'w';
			out->instruction[2] = '\0';
			out->rd = ((1 << 5) - 1) & (in->instruction >> 7);
			out->rs1 = ((1 << 5) - 1) & (in->instruction >> 15);
			out->imm = ((1 << 11) - 1) & (in->instruction >> 20);
			break;
		case 0b0010011:
			funct3Mask = ((1 << 3) - 1) & (in->instruction >> 12);
			switch (funct3Mask) {
				case 0b000:
					//addi
					out->instruction[0] = 'a';
					out->instruction[1] = 'd';
					out->instruction[2] = 'd';
					out->instruction[3] = 'i';
					out->instruction[4] = '\0';
					out->rd = ((1 << 5) - 1) & (in->instruction >> 7);
					out->rs1 = ((1 << 5) - 1) & (in->instruction >> 15);
					out->imm = ((1 << 11) - 1) & (in->instruction >> 20);
					break;
				case 0b001:
					//slli
					out->instruction[0] = 's';
					out->instruction[1] = 'l';
					out->instruction[2] = 'l';
					out->instruction[3] = 'i';
					out->instruction[4] = '\0';
					out->rd = ((1 << 5) - 1) & (in->instruction >> 7);
					out->rs1 = ((1 << 5) - 1) & (in->instruction >> 15);
					out->imm = ((1 << 11) - 1) & (in->instruction >> 20);
					break;
				case 0b010:
					//slti
					out->instruction[0] = 's';
					out->instruction[1] = 'l';
					out->instruction[2] = 't';
					out->instruction[3] = 'i';
					out->instruction[4] = '\0';
					out->rd = ((1 << 5) - 1) & (in->instruction >> 7);
					out->rs1 = ((1 << 5) - 1) & (in->instruction >> 15);
					out->imm = ((1 << 11) - 1) & (in->instruction >> 20);
					break;
				case 0b100:
					//xori
					out->instruction[0] = 'x';
					out->instruction[1] = 'o';
					out->instruction[2] = 'r';
					out->instruction[3] = 'i';
					out->instruction[4] = '\0';
					out->rd = ((1 << 5) - 1) & (in->instruction >> 7);
					out->rs1 = ((1 << 5) - 1) & (in->instruction >> 15);
					out->imm = ((1 << 11) - 1) & (in->instruction >> 20);
					break;
				case 0b101:
					funct7Mask = ((1 << 7) - 1) & (in->instruction >> 25);
					switch (funct7Mask) {
						case 0b0000000:
							//srli
							out->instruction[0] = 's';
							out->instruction[1] = 'r';
							out->instruction[2] = 'l';
							out->instruction[3] = 'i';
							out->instruction[4] = '\0';
							out->rd = ((1 << 5) - 1) & (in->instruction >> 7);
							out->rs1 = ((1 << 5) - 1) & (in->instruction >> 15);
							out->imm = ((1 << 11) - 1) & (in->instruction >> 20);
							break;
						case 0b0100000:
							//srai
							out->instruction[0] = 's';
							out->instruction[1] = 'r';
							out->instruction[2] = 'a';
							out->instruction[3] = 'i';
							out->instruction[4] = '\0';
							out->rd = ((1 << 5) - 1) & (in->instruction >> 7);
							out->rs1 = ((1 << 5) - 1) & (in->instruction >> 15);
							out->imm = ((1 << 11) - 1) & (in->instruction >> 20);
							break;
					}
					break;
				case 0b110:
					//ori
					out->instruction[0] = 'o';
					out->instruction[1] = 'r';
					out->instruction[2] = 'i';
					out->instruction[3] = '\0';
					out->rd = ((1 << 5) - 1) & (in->instruction >> 7);
					out->rs1 = ((1 << 5) - 1) & (in->instruction >> 15);
					out->imm = ((1 << 11) - 1) & (in->instruction >> 20);
					break;
				case 0b111:
					//andi
					out->instruction[0] = 'a';
					out->instruction[1] = 'n';
					out->instruction[2] = 'd';
					out->instruction[3] = 'i';
					out->instruction[4] = '\0';
					out->rd = ((1 << 5) - 1) & (in->instruction >> 7);
					out->rs1 = ((1 << 5) - 1) & (in->instruction >> 15);
					out->imm = ((1 << 11) - 1) & (in->instruction >> 20);
					break;
			}
			break;
		case 0b0010111:
			//auipc
			out->instruction[0] = 'a';
			out->instruction[1] = 'u';
			out->instruction[2] = 'i';
			out->instruction[3] = 'p';
			out->instruction[4] = 'c';
			out->instruction[5] = '\0';
			out->rd = ((1 << 5) - 1) & (in->instruction >> 7);
			out->imm = ((1 << 20) - 1) & (in->instruction >> 12);
			break;
		case 0b0100011:
			//sw
			out->instruction[0] = 's';
			out->instruction[1] = 'w';
			out->instruction[2] = '\0';
			out->imm = (((1 << 5) - 1) & (in->instruction >> 7)) & (((1 << 7) - 1) & (in->instruction >> 25) << 5);
			out->rs1 = ((1 << 5) - 1) & (in->instruction >> 15);
			out->rs2 = ((1 << 5) - 1) & (in->instruction >> 20);
			break;
		case 0b0110011:
			funct3Mask = ((1 << 3) - 1) & (in->instruction >> 12);
			switch (funct3Mask) {
				case 0b000:
					funct7Mask = ((1 << 7) - 1) & (in->instruction >> 25);
					switch (funct7Mask) {
						case 0b0000000:
							//add
							out->instruction[0] = 'a';
							out->instruction[1] = 'd';
							out->instruction[2] = 'd';
							out->instruction[3] = '\0';
							out->rd = ((1 << 5) - 1) & (in->instruction >> 7);
							out->rs1 = ((1 << 5) - 1) & (in->instruction >> 15);
							out->rs2 = ((1 << 5) - 1) & (in->instruction >> 20);
							break;
						case 0b0100000:
							//sub
							out->instruction[0] = 's';
							out->instruction[1] = 'u';
							out->instruction[2] = 'b';
							out->instruction[3] = '\0';
							out->rd = ((1 << 5) - 1) & (in->instruction >> 7);
							out->rs1 = ((1 << 5) - 1) & (in->instruction >> 15);
							out->rs2 = ((1 << 5) - 1) & (in->instruction >> 20);
							break;
					}
					break;
				case 0b001:
					//sll
					out->instruction[0] = 's';
					out->instruction[1] = 'l';
					out->instruction[2] = 'l';
					out->instruction[3] = '\0';
					out->rd = ((1 << 5) - 1) & (in->instruction >> 7);
					out->rs1 = ((1 << 5) - 1) & (in->instruction >> 15);
					out->rs2 = ((1 << 5) - 1) & (in->instruction >> 20);
					break;
				case 0b010:
					//slt
					out->instruction[0] = 's';
					out->instruction[1] = 'l';
					out->instruction[2] = 't';
					out->instruction[3] = '\0';
					out->rd = ((1 << 5) - 1) & (in->instruction >> 7);
					out->rs1 = ((1 << 5) - 1) & (in->instruction >> 15);
					out->rs2 = ((1 << 5) - 1) & (in->instruction >> 20);
					break;
				case 0b100:
					//xor
					out->instruction[0] = 'x';
					out->instruction[1] = 'o';
					out->instruction[2] = 'r';
					out->instruction[3] = '\0';
					out->rd = ((1 << 5) - 1) & (in->instruction >> 7);
					out->rs1 = ((1 << 5) - 1) & (in->instruction >> 15);
					out->rs2 = ((1 << 5) - 1) & (in->instruction >> 20);
					break;
				case 0b101:
					funct7Mask = ((1 << 7) - 1) & (in->instruction >> 25);
					switch (funct7Mask) {
						case 0b0000000:
							//srl
							out->instruction[0] = 's';
							out->instruction[1] = 'r';
							out->instruction[2] = 'l';
							out->instruction[3] = '\0';
							out->rd = ((1 << 5) - 1) & (in->instruction >> 7);
							out->rs1 = ((1 << 5) - 1) & (in->instruction >> 15);
							out->rs2 = ((1 << 5) - 1) & (in->instruction >> 20);
							break;
						case 0b0100000:
							//sra
							out->instruction[0] = 's';
							out->instruction[0] = 'r';
							out->instruction[0] = 'a';
							out->instruction[0] = '\0';
							out->rd = ((1 << 5) - 1) & (in->instruction >> 7);
							out->rs1 = ((1 << 5) - 1) & (in->instruction >> 15);
							out->rs2 = ((1 << 5) - 1) & (in->instruction >> 20);
							break;
					}
					break;
				case 0b110:
					//or
					out->instruction[0] = 'o';
					out->instruction[1] = 'r';
					out->instruction[2] = '\0';
					out->rd = ((1 << 5) - 1) & (in->instruction >> 7);
					out->rs1 = ((1 << 5) - 1) & (in->instruction >> 15);
					out->rs2 = ((1 << 5) - 1) & (in->instruction >> 20);
					break;
				case 0b111:
					//and
					out->instruction[0] = 'a';
					out->instruction[1] = 'n';
					out->instruction[2] = 'd';
					out->instruction[3] = '\0';
					out->rd = ((1 << 5) - 1) & (in->instruction >> 7);
					out->rs1 = ((1 << 5) - 1) & (in->instruction >> 15);
					out->rs2 = ((1 << 5) - 1) & (in->instruction >> 20);
					break;
			}
			break;
		case 0b0110111:
			//lui
			out->instruction[0] = 'l';
			out->instruction[1] = 'u';
			out->instruction[2] = 'i';
			out->instruction[3] = '\0';
			out->rd = ((1 << 5) - 1) & (in->instruction >> 7);
			out->imm = ((1 << 20) - 1) & (in->instruction >> 12);
			break;
		case 0b1100011:
			funct3Mask = ((1 << 3) - 1) & (in->instruction >> 12);
			switch (funct3Mask) {
				case 0b000:
					//beq
					out->instruction[0] = 'b';
					out->instruction[1] = 'e';
					out->instruction[2] = 'q';
					out->instruction[3] = '\0';
					out->imm = (((1 << 5) - 1) & (in->instruction >> 7)) & (((1 << 7) - 1) & (in->instruction >> 25) << 5);
					out->rs1 = ((1 << 5) - 1) & (in->instruction >> 15);
					out->rs2 = ((1 << 5) - 1) & (in->instruction >> 20);
					break;
				case 0b001:
					//bne
					out->instruction[0] = 'b';
					out->instruction[1] = 'n';
					out->instruction[2] = 'e';
					out->instruction[3] = '\0';
					out->imm = (((1 << 5) - 1) & (in->instruction >> 7)) & (((1 << 7) - 1) & (in->instruction >> 25) << 5);
					out->rs1 = ((1 << 5) - 1) & (in->instruction >> 15);
					out->rs2 = ((1 << 5) - 1) & (in->instruction >> 20);
					break;
			}
			break;
		case 0b1100111:
			//jalr
			out->instruction[0] = 'j';
			out->instruction[1] = 'a';
			out->instruction[2] = 'l';
			out->instruction[3] = 'r';
			out->instruction[4] = '\0';
			out->rd = ((1 << 5) - 1) & (in->instruction >> 7);
			out->rs1 = ((1 << 5) - 1) & (in->instruction >> 15);
			out->imm = ((1 << 11) - 1) & (in->instruction >> 20);
			break;
		case 0b1101111:
			//jal
			out->instruction[0] = 'j';
			out->instruction[1] = 'a';
			out->instruction[2] = 'l';
			out->instruction[3] = '\0';
			out->rd = ((1 << 5) - 1) & (in->instruction >> 7);
			out->imm = ((1 << 20) - 1) & (in->instruction >> 12);
			break;
		case 0b1110011:
			//ecall
			out->instruction[0] = 'e';
			out->instruction[1] = 'c';
			out->instruction[2] = 'a';
			out->instruction[3] = 'l';
			out->instruction[4] = 'l';
			out->instruction[5] = '\0';
			out->rd = ((1 << 5) - 1) & (in->instruction >> 7);
			out->rs1 = ((1 << 5) - 1) & (in->instruction >> 15);
			break;
	}
	
	out->next_pc = in->next_pc;
	
	return 0;
}

int execute( struct ID_EX_buffer *in, struct EX_MEM_buffer *out )
{
	
	//Case statement for each instruction
	switch (in->instruction[0]) {
		case 'a':
			switch (in->instruction[1]) {
				case 'd':
					switch (in->instruction[3]) {
						case '\0':
							//add
							out->data = in->rs1 + in->rs2;
							out->rd = in->rd;
							out->memBoolean = 0;
							break;
						case 'i':
							//addi
							out->data = in->rs1 + in->imm;
							out->rd = in->rd;
							out->memBoolean = 0;
							break;
					}
					break;
				case 'n':
					switch (in->instruction[3]) {
						case '\0':
							//and
							out->data = in->rs1 & in->rs2;
							out->rd = in->rd;
							out->memBoolean = 0;
							break;
						case 'i':
							//andi
							out->data = in->rs1 & in->imm;
							out->rd = in->rd;
							out->memBoolean = 0;
							break;
					}
					break;
				case 'u':
					//auipc
					out->data = in->next_pc + in->imm;
					out->rd = in->rd;
					out->memBoolean = 0;
					break;
			}
			break;
		case 'b':
			switch (in->instruction[1]) {
				case 'e':
					//beq
					if (in->rs1 == in->rs2) {
						out->data = in->imm;
					}
					else {
						out->data = 0;
					}
					out->memBoolean = 0;
					break;
				case 'n':
					//bne
					if (in->rs1 != in->rs2) {
						out->data = in->imm;
					}
					else {
						out->data = 0;
					}
					out->memBoolean = 0;
					break;
			}
			break;
		case 'e':
			//ecall
			out->data = in->rs1;
			out->rd = in->rd;
			out->memBoolean = 0;
			break;
		case 'j':
			switch (in->instruction[3]) {
				case '\0':
					//jal
					out->data = in->next_pc + in->imm;
					out->rd = in->rd;
					out->memBoolean = 0;
					break;
				case 'r':
					//jalr
					out->data = in->next_pc + (in->rs1 + in->imm & ~1);
					out->rd = in->rd;
					out->memBoolean = 0;
					break;
			}
			break;
		case 'l':
			switch (in->instruction[1]) {
				case 'u':
					//lui
					out->data = in->imm;
					out->rd = in->rd;
					out->memBoolean = 1;
					break;
				case 'w':
					//lw
					out->data = in->rs1 + in->imm;
					out->rd = in->rd;
					out->memBoolean = 1;
					break;
			}
			break;
		case 'o':
			switch (in->instruction[2]) {
				case '\0':
					//or
					out->data = in->rs1 | in->rs2;
					out->rd = in->rd;
					out->memBoolean = 0;
					break;
				case 'i':
					//ori
					out->data = in->rs1 | in->imm;
					out->rd = in->rd;
					out->memBoolean = 0;
					break;
			}
			break;
		case 's':
			switch (in->instruction[1]) {
				case 'l':
					switch (in->instruction[2]) {
						case 't':
							switch (in->instruction[3]) {
								case '\0':
									//slt
									out->data = (in->rs1 < in->rs2);
									out->rd = in->rd;
									out->memBoolean = 1;
									break;
								case 'i':
									//slti
									out->data = (in->rs1 < in->imm);
									out->rd = in->rd;
									out->memBoolean = 1;
									break;
							}
							break;
						case 'l':
							switch (in->instruction[3]) {
								case '\0':
									//sll
									out->data = in->rs1 << in->rs2;
									out->rd = in->rd;
									out->memBoolean = 1;
									break;
								case 'i':
									//slli
									out->data = in->rs1 << in->imm;
									out->rd = in->rd;
									out->memBoolean = 1;
									break;
							}
							break;
					}
					break;
				case 'r':
					switch (in->instruction[2]) {
						case 'a':
							switch (in->instruction[3]) {
								case '\0':
									//sra
									out->data = in->rs1 >> in->rs2;
									out->rd = in->rd;
									out->memBoolean = 1;
									break;
								case 'i':
									//srai
									out->data = in->rs1 >> in->imm;
									out->rd = in->rd;
									out->memBoolean = 1;
									break;
							}
							break;
						case 'l':
							switch (in->instruction[3]) {
								case '\0':
									//srl
									out->data = in->rs1 >> in->rs2;
									out->rd = in->rd;
									out->memBoolean = 1;
									break;
								case 'i':
									//srli
									out->data = in->rs1 >> in->imm;
									out->rd = in->rd;
									out->memBoolean = 1;
									break;
							}
							break;
					}
					break;
				case 'u':
					//sub
					out->data = in->rs1 - in->rs2;
					out->rd = in->rd;
					out->memBoolean = 0;
					break;
				case 'w':
					//sw
					out->data = in->rs1 + in->imm;
					out->rd = in->rs2;
					out->memBoolean = 1;
					break;
			}
			break;
		case 'x':
			switch (in->instruction[3]) {
				case '\0':
					//xor
					out->data = in->rs1 ^ in->rs2;
					out->rd = in->rd;
					out->memBoolean = 0;
					break;
				case 'i':
					//xori
					out->data = in->rs1 ^ in->imm;
					out->rd = in->rd;
					out->memBoolean = 0;
					break;
			}
			break;
	}

	out->next_pc = in->next_pc;
	
	return 0;
}

int memory( struct EX_MEM_buffer *in, struct MEM_WB_buffer *out )
{
	/*
		This checks if returned boolean is one in order to 
		call this function. 
		It will only use this if there is a store
		or load instruction.
	*/
	if(in->memBoolean == 1){
		out->rd = in->rd;
		out->data = in->data;
		out->next_pc = in->next_pc;
	}
	return 0;
}

int writeback( struct MEM_WB_buffer *in )
{
	//update the GPR with new data
	for (int i=0; i<cpu_ctx.GPR[32]; i++){
		if(cpu_ctx.GPR[i]== in->rd){
			cpu_ctx.GPR[i]== in->data;
		}
	}
	
	return 0;
}


